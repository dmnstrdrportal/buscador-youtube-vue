import { shallowMount, createLocalVue } from '@vue/test-utils'
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
import Search from '@/components/Search.vue'

const localVue = createLocalVue();
localVue.use(BootstrapVue);
localVue.use(IconsPlugin);


describe('Search.vue', () => {
    const wrapper = shallowMount(Search, {
        localVue
    })
    it('Valida existencia de botones iniciales', () => {
        expect(wrapper.find('#searchVideoButton').exists()).toBe(true);
        expect(wrapper.find('#clearTextButton').exists()).toBe(true);
    })

    it('Validacion de resultado botón buscar', async () => {
        //Falta inyectar objetos mocks para no validar directamente con la API
        const buttonSearch = wrapper.find('#searchVideoButton')
        expect(wrapper.vm.items.length).toBe(0)
        buttonSearch.trigger('click')
        await tick();
        expect(wrapper.vm.items.length).toBe(5);
    })

    it('Valida existencia de botones del páginado', () => {
        expect(wrapper.find('#prevButton').exists()).toBe(true);
        expect(wrapper.find('#nextButton').exists()).toBe(true);
    })
})

//Método que define un periodo de tiempo en espera de respuesta de API
function tick() {
    return new Promise(resolve => {
        setTimeout(resolve, 3000);
    })
}
