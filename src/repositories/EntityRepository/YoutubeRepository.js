import Repository from "../Repository"

//APIs
const youtube = 'apiYoutube';

//Resources
const resource = 'search';

/**
 * Obtener resultado de videos en base a un valor ingresado
 */

export default {
    getResult(value, pageToken) {
        var urlPage = '';
        if (pageToken)
            urlPage = `&pageToken=${pageToken}`;

        return Repository(youtube).get(`${resource}/?part=snippet&order=viewCount&q=${value}&type=video&videoDefinition=high&key=${process.env.VUE_APP_API_KEY}${urlPage}`)
    }
}