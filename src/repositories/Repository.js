import axios from 'axios'

const apis = {
    apiYoutube: `https://youtube.googleapis.com/youtube/v3/`
}

const sendReq = (apiName) => {
    return axios.create({
            baseURL: `${apis[apiName]}`
    })
}

export default sendReq;