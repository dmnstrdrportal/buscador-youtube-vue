# Buscador Youtube Vue
Aplicación que entrega resultados de videos de Youtube en base a lo ingresado por el buscador

## Instalación

Se debe ejecutar el siguiente comando para instalar las referencias del proyecto

```
npm install
```

## Utilización

```
npm run serve
```

## Pruebas unitarias

```
npm run test
```

## Versiones

* v0.1.1: Versión Inicial
